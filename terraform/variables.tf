variable "name" {
  default = "test"
}

variable "environment" {
  default = "test"
}
variable "az" {
  description = "AZ for subnet"
  default     = "eu-west-3a"
}

//variable "aws_access_key" {
//  default = "xxxxxxxxxxxxx"
//}

//variable "aws_secret_key" {
//  default = "yyyyyyyyyyyyyyyyyyyyyy"
//}

//variable "aws_key_path" {
//  default = "/home/vagrant/"
//}

//variable "aws_key_name" {
//  default = "key_pair"
//}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-west-3"
}

variable "ami" {
  description = "AMI by region"
  #default = "ami-08d3d8790db280475"
  default = "ami-0cea55c8f7bcd2571"
  
}
variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "10.0.0.0/16"
}
variable "public_subnet_cidr" {
  description = "CIDR for the Public Subnet"
  default     = "10.0.10.0/24"
}
variable "private_subnet_a_cidr" {
  description = "CIDR for the Private Subnet A"
  default     = "10.0.1.0/24"
}
variable "private_subnet_b_cidr" {
  description = "CIDR for Private Subnet B"
  default     = "10.0.2.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for private subnet"
  default     = "10.0.1.0/24"
}

