resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tmpl", {
      bastion_ip          = aws_instance.bastion.public_ip,
      k8s_ip              = aws_instance.k8s_node.private_ip
  })
  filename = "inventory"
}
