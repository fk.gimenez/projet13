resource "local_file" "private_key" {
  sensitive_content = tls_private_key.key.private_key_pem
  filename          = format("%s/%s/%s", abspath(path.root), ".ssh", "ssh-key.pem")
  file_permission   = "0600"
}

resource "local_file" "ansible_ssh_access" {
  content = templatefile("ssh.tmpl", {
      bastion_ip          = aws_instance.bastion.public_ip,
      k8s_ip              = aws_instance.k8s_node.private_ip,
      ssh_keyfile         = local_file.private_key.filename
  })
  filename = "ssh2.cfg"
}
