resource "local_file" "ami" {
  content = templatefile("ami.tmpl", {
      k8s_id              = aws_instance.k8s_node.id
  })
  filename = "ami"
}
