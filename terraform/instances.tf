resource "tls_private_key" "key" {
 algorithm = "RSA"
 rsa_bits  = 4096
}

resource "aws_key_pair" "aws_key" {
 key_name   = "ssh-key"
 public_key = tls_private_key.key.public_key_openssh
}

resource "aws_instance" "bastion" {
  ami                         = var.ami
  availability_zone           = "eu-west-3a"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.aws_key.key_name
  vpc_security_group_ids      = [aws_security_group.bastion_SG.id]
  subnet_id                   = aws_subnet.public.id
  associate_public_ip_address = true
  source_dest_check           = false
  tags = {
    Name = "Bastion"
  }
}
output "bastion_public_ip" {
  value       = aws_instance.bastion.public_ip
  description = "The public IP of the bastion"
}


resource "aws_instance" "k8s_node" {
  ami                         = var.ami
  availability_zone           = "eu-west-3a"
  instance_type               = "t3.micro"
  key_name                    = aws_key_pair.aws_key.key_name
  vpc_security_group_ids      = [aws_security_group.k8s_SG.id]
  subnet_id                   = aws_subnet.private.id
  associate_public_ip_address = true
  source_dest_check           = false
  tags = {
    Name = "k8sNodeInstance"
  }
}
output "k8s_node_public_ip" {
  value       = aws_instance.k8s_node.public_ip
  description = "The public IP of the k8sNode"
}
output "eks_node_private_ip" {
  value       = aws_instance.k8s_node.private_ip
  description = "The private IP of the k8sNode"
}
