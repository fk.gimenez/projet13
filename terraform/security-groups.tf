resource "aws_security_group" "bastion_SG" {
  vpc_id      = aws_vpc.main_vpc.id
  name        = "Bastion Security Group"
  description = "Bastion Security Group"

  tags = {
    Name = "Bastion_SG"
  }
}

resource "aws_security_group_rule" "http_to_bastion_ingress_rule" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.bastion_SG.id
}
resource "aws_security_group_rule" "bastion_allow_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.bastion_SG.id
}


resource "aws_security_group" "k8s_SG" {
  vpc_id      = aws_vpc.main_vpc.id
  name        = "k8s Security Group"
  description = "k8s Security Group"

  tags = {
    Name = "k8s_SG"
  }
}
resource "aws_security_group_rule" "bastion_to_k8s_ingress_rule" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.bastion_SG.id
  security_group_id        = aws_security_group.k8s_SG.id
}
resource "aws_security_group_rule" "k8s_allow_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.k8s_SG.id
}


